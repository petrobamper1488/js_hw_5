function createNewUser() {
  let userFirstName;
    do {
      userFirstName = prompt("Введіть Ваше ім'я:");
    } while (!userFirstName || !/^[A-Za-z]+$/.test(userFirstName));

  let userLastName;
    do {
      userLastName = prompt("Введіть Ваше прізвище:");
    } while (!userLastName || !/^[a-zA-Z\s]+$/.test(userLastName));

  let newUser = {
      _firstName: userFirstName,
      _lastName: userLastName,
      set firstName (value) {
        if (typeof value === "string") {
        this._firstName = value;
        }
      },
      get firstName () {
        return this._firstName;
      },
      set lastName (value) {
        if (typeof value === "string") {
        this._lastName = value;
        }
      },
      get lastName () {
        return this._lastName;
      },
      getLogin: function () {
      return (this._firstName.charAt(0) + this._lastName).toLowerCase();
      }
  };
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());